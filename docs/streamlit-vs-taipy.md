# Streamlit vs. Taipy - 궁극의 비교 <sup>[1](#footnote_1)</sup>

최근 두 편의 글 [Taipy GUI: It is specifically used to build GUI applications](https://medium.com/p/1706d6974a37/)와 [Taipy Core: It provides an advanced backend for data-driven applications tailored to business use cases](https://medium.com/p/22922714751b)에서 low code로 안정적인 데이터 기반 파이프라인의 생성, 관리와 실행을 간소화하는 오픈 소스 도구인 [Taipy](https://bit.ly/3o5yXsd)에 대한 포괄적인 개요를 소개한 바 있다.

![](./images/1_TNUCXTyWOetTR9jrzefASg.webp)

- **Taipy GUI**를 사용하면 아주 적은 코드로 복잡한 대화형 GUI를 만들 수 있다.
- **Taipy Core**를 사용하면 복잡한 대화형 데이터 파이프라인을 만들 수 있다.

이 시점에서 유사한 솔루션, 특히 Python 스크립트를 웹 앱으로 전환할 수 있는 [Streamlit](https://streamlit.io/)과 비교하여 얼마나 우수한지 평가하고자 한다.

보다 구체적으로, 실제 솔루션에 대한 실제 적용 가능성과 밀접하게 일치하는 다양한 매개 변수를 기준으로 평가하고 두 솔루션이 어떻게 차별화되는지 살펴본다.

시작하자!

## Prototyping

**Streamlit**

말 그대로 코드가 거의 없는 Streamlit으로 어플리케이션을 디자인하는 것은 매우 간단하다.

따라서 프로토타이핑을 위한 탁월한 선택이다.

Python 구문에 대한 기본적인 이해가 있는 사람이라면 누구나 Streamlit으로 어플리케이션을 개발할 수 있다.

그만큼 간단하고 깔끔하다.

**Taipy**

Taipy는 어플리케이션 개발을 위해 새로운 접근 방식을 취하고 있다.

[Taipy GUI 문서](https://medium.com/p/1706d6974a37/)에서 설명한 것처럼 Taipy는 확장된 마크다운 구문을 제공한다.

```python
from taipy import Gui

name = "Avi Chawla"

page = """
# First Taipy Web Application.

Hello: <|{name}|>
"""

Gui(page=page).run()
```

위에서 처럼 마크다운 구문을 사용하여 어플리케이션에 시각적 요소를 추가할 수 있다.

![](./images/0_TNeivJhHMOO7Ijbn.webp)

마크다운은 간단하고 명확하며 우아하고 개발하기 쉽다. 하지만 텍스트를 굵게 표시하고, 하이퍼링크를 추가하고, 시각적 요소를 추가하고, 매개변수를 지정하는 방법 등 마크다운에 어느 정도 익숙해져야 할 수도 있다.

간단한 프로토타이핑에는 개인적으로 Streamlit이 좋다고 생각한다.

다른 중요한 면에서도 StreamLit과 Taipy를 비교 평가해 보자.

## 콜백(Callbacks)

**Streamlit**
일단 streamlit 어플리케이션을 실행하면 사용자가 상호작용할 때 발생하는 그래픽 이벤트를 거의 제어할 수 없다.

즉, Streamlit에는 캐시되지 않은 **모든 그래픽 컴포넌트**를 트리거하고 다시 렌더링하는 글로벌 이벤트 루프가 있기 때문이다.

![](./images/1_hqJUp1Y0M3WUwHOc-pupcw.webp)

실제로 다양한 토론 플랫폼에서 많은 사람들이 이 문제를 제기했다.

> [![](./images/streamlit_sucks.png)](https://www.reddit.com/r/Streamlit/comments/w0jub2/streamlit_sucks/?utm_content=body&utm_medium=post_embed&utm_name=6139ac1d40f24e1cb273ab8030dc3b54&utm_source=embedly&utm_term=w0jub2)

특히 프로덕션 레벨 어플리케이션의 경우 어플리케이션의 상호 작용을 심각하게 제한하고 있다.

**Taipy**
하지만 Taipy는 편리한 웹 앱 개발 경험을 제공한다.

기본적으로 사용자가 시각적 요소와 상호 작용할 때 Taipy는 GUI의 동작 또는 변경 사항에 따라 콜백을 트리거한다.

![](./images/1_-hx8H21bDaFLoldx_UrQzg.webp)

간단히 말해 콜백은 페이지에 대한 사용자의 상호작용에 대한 응답으로 호출되는 함수이다.

따라서 시각적 요소를 변수에 바인딩하고 더 나아가 변수를 콜백 함수에 바인딩할 수 있다.

![](./images/1_d3JxKdHoOGzi1nu_OQaHHQ.webp)

결과적으로 전체 페이지를 캐시하거나 다시 로드할 필요가 없다. 따라서 어플리케이션의 성능이 향상된다.

## 설계 유연성
Streamlit은 웹 앱을 위한 표준 디자인 템플릿을 제공한다.

즉, CSS, HTML, 글꼴 등을 사용하여 앱의 레이아웃을 변경하는 것이 매우 어렵다.

![](./images/1_zSlXHjRY48g_yq0XKzgkIQ.webp)

따라서 각 Streamlit 어플리케이션은 동일하게 보인다.

이는 Streamlit이 주로 일부 인터랙티브 기능과 차트가 포함된 프로토타입을 만들도록 설계되었기 때문이다.

**Taipy**
하지만 Taipy는 매우 유연하다.

즉, CSS에 대한 사전 지식이 없어도 텍스트, 버튼, 차트 등 모든 시각적 요소의 표시 속성을 쉽게 변경할 수 있다.

![](./images/1_XqO4_zu1EhgPD6Koe0ZDFQ.webp)

따라서 각 Taipy 어플리케이션은 다르게 보일 수 있다.

이는 Taipy가 프로토타입 구축뿐만 아니라 완전한 데이터 기반 어플리케이션을 구축할 수 있도록 설계되었기 때문이다.

자세한 정보는 여기를 참조하세요: [Taipy Stylekit](https://bit.ly/47dq9SY).

## 빅 데이터 지원

![](./images/1_6wXqPd-GteQgl_D5EBpO3w.webp)

**Streamlit**
안타깝게도 Streamlit에는 대량의 데이터를 처리할 수 있는 지원과 최적화 기능이 내장되어 있지 않다.

![](./images/1_IQBW7oDdwqO7WUbOG38AfA.webp)

즉, 데이터에 관계없이 각 Streamlit 어플리케이션은 백그라운드에서 동일한 방식으로 컴파일되고 빌드된다.

**Taipy**
Taipy를 통해 대량의 데이터를 보다 쉽게 처리할 수 있는 여러 가지 방법이 있다.

![](./images/1_2xh_grZVpFAFZr-zOO0oCQ.webp)

지원되는 일부 기능들은 다음과 같다.

- 표의 페이지 매김 - Streamlit에서 기본적으로 지원되지는 않지만, 여기에 표시된 것처럼 약간의 노력을 기울이면 수동으로 조정할 수 있다.
- [차트용 데시메이터](https://docs.taipy.io/en/develop/manuals/reference/taipy.gui.data.Decimator/?utm_source=INF&utm_medium=Linkedin&utm_campaign=avi) - Taipy는 전체 모양을 유지하면서 차트에 표시되는 데이터 포인트의 수를 자동으로 줄인다. 이렇게 하면 특히 큰 데이터세트에서 시간과 메모리를 절약할 수 있다.
- 비동기 실행 - Taipy에서는 함수를 동기 또는 비동기로 실행할 수 있다. 이는 최종 사용자가 실행이 완료되기를 기다리는 동안 'stuck'이 발생하지 않도록 하는 'long runs'에 매우 중요하다. 이는 동기식으로만 실행되는 Streamlit와 대조적이다.

빅 데이터와 대용량 처리를 위하여 위의 기능들은 Streamlit보다 더 나은 기능을 제공한다.

## 전용 백엔드 프레임워크

**Streamlit**
주로 간단한 프런트엔드 프레임워크를 사용하여 어플리케이션을 제어하도록 Streamlit은 설계되었다.

즉, 다양한 시각적 요소를 사용하여 어플리케이션을 코딩하도록 되어 있다.

Streamlit의 이러한 최소한의 특성으로 인해 프로토타이핑에 선호되는 선택이지만 백엔드를 제어할 수는 없다.

**Taipy**
Taipy는 어플리케이션의 백엔드 생성을 위한 전용된 지원을 제공한다.

이를 통해 모든 어플리케이션을 쉽고 효율적으로 생성하고, 구성하며, 시나리오를 처리하며, 파이프라인 및 버전 관리를 수행 할 수 있다.

최신 버전인 Taipy 2.3에는 핵심 시각적 요소가 포함되어 있다. 이를 통해 프론트엔드와 백엔드를 훨씬 더 쉽게 연결할 수 있다.

이에 대해서는 [Taipy Core 문서](https://medium.com/p/22922714751b)에서 자세히 설명하고 있다.

## 마치며
이번 포스팅에서는 Streamlit과 Taipy의 몇 가지 주요 차이점을 알아보았다.

특히 유연성, 빅 데이터 지원, 앱 실행 등 다양한 관점을 기준으로 두 제품을 평가하여 데이터 팀에 얼마나 적합한지 알아보았다.

다른 두 글에서 Taipy GUI와 Taipy 코어에 대해 다루고 있다.

- [Taipy GUI: It is specifically used to build GUI applications.](https://medium.com/p/1706d6974a37/)
- [Taipy Core: It provides an advanced backend for data-driven applications tailored to business use cases.](https://medium.com/p/22922714751b)


<a name="footnote_1">1</a>: 이 페이지는 [Streamlit vs. Taipy — The Ultimate Comparison](https://towardsdev.com/streamlit-vs-taipy-the-ultimate-comparison-f705d60930f6)을 편역한 것임.
